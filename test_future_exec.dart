// Test future is execute or not if we don't call then

Future<int> create() {
  return Future.delayed(Duration(seconds: 2), () {
    print("called in computation");
    return 1;
  });
}

main(List<String> args) {
  // 说不不调用then。也是会执行。
  create();
}
