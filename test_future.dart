Future<String> createFuture() {
  return Future.delayed(Duration(seconds: 3), () => "Hello");
}

main(List<String> args) {
  print("before");
  createFuture().then((value) {
    print(value);
    return " World";
  });
  print("after");
  // hahah we will wait for future here.
}
